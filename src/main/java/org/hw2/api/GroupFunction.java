package org.hw2.api;

import java.util.List;
import java.util.Map;

public interface GroupFunction {
    public Integer uniqueWords();
    public void frequencyOfWords();
    public List<String> reverseLine();
    public void printByLine(String lines);

    public List<Map.Entry<String, Integer>> printUniqueLines();
    public List<Map.Entry<String, Integer>> printUniqueWords();
}
