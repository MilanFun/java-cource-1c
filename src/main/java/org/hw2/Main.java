package org.hw2;

import org.hw2.impl.GroupFunctionImpl;
import org.hw2.impl.Transform;

import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        GroupFunctionImpl groupFunction = new GroupFunctionImpl.Builder(Transform.transform("/home/aleksey/text"))
                                                               .setMapLine()
                                                               .setMapWord()
                                                               .build();
        System.out.println("Frequency words");
        groupFunction.frequencyOfWords();
        System.out.println("Unique quantity of words");
        System.out.println(groupFunction.uniqueWords());
        System.out.println("Unique words");
        for(Map.Entry<String, Integer> entry : groupFunction.printUniqueWords()) {
            System.out.println(entry.getKey());
        }
        System.out.println("Unique lines");
        for(Map.Entry<String, Integer> entry : groupFunction.printUniqueLines()) {
            System.out.println(entry.getKey());
        }
        System.out.println("Reverse line");
        System.out.println(groupFunction.reverseLine());

        Iterator<String> iter = groupFunction.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        Scanner scanner = new Scanner(System.in);
        groupFunction.printByLine(scanner.nextLine());
        scanner.close();
    }
}
