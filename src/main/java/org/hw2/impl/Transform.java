package org.hw2.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Transform {
    public static List<String> transform(String path) {
        List<String> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(path), "UTF8"))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                StringBuilder builder = new StringBuilder();
                int count = 0;
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == '.' || line.charAt(i) == ',' ) {
                        builder.append("");
                    }
                    builder.append(line.charAt(i));
                    count += 1;
                    if (count % 10 == 0) {
                        list.add(builder.toString().trim());
                        builder.delete(0, count);
                        count = 0;
                    }
                }
                builder = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
