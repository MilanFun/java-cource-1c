package org.hw2.impl;

import org.hw2.api.GroupFunction;

import java.util.*;

public class GroupFunctionImpl implements GroupFunction, Iterable<String> {
    private List<String> listOfLines;
    private Map<String, Integer> mapWord;
    private Map<String, Integer> mapLine;

    public GroupFunctionImpl(Builder builder) {
        this.listOfLines = builder.listOfLines;
        this.mapWord = builder.mapWord;
        this.mapLine = builder.mapLine;
    }

    public static class Builder {
        private List<String> listOfLines;
        private Map<String, Integer> mapWord;
        private Map<String, Integer> mapLine;

        public Builder(List<String> listOfLines) {
            this.listOfLines = listOfLines;
        }

        public Builder setMapWord() {
            this.mapWord = new HashMap<>();
            for(String elem : this.listOfLines) {
                for (String subElem : elem.split(" ")) {
                    if (!this.mapWord.containsKey(subElem)) {
                        this.mapWord.put(subElem, 1);
                    } else {
                        this.mapWord.put(subElem, this.mapWord.get(subElem) + 1);
                    }
                }
            }
            return this;
        }

        public Builder setMapLine() {
            this.mapLine = new HashMap<>();
            for(String elem : this.listOfLines) {
                if (!this.mapLine.containsKey(elem)) {
                    this.mapLine.put(elem, 1);
                } else {
                    this.mapLine.put(elem, this.mapLine.get(elem) + 1);
                }
            }
            return this;
        }

        public GroupFunctionImpl build() {
            return new GroupFunctionImpl(this);
        }
    }

    @Override
    public Integer uniqueWords() {
        return this.mapWord.size();
    }

    private Comparator<Map.Entry<String, Integer>> keyComparator() {
        return new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> stringIntegerEntry, Map.Entry<String, Integer> t1) {
                if (stringIntegerEntry.getKey().length() == t1.getKey().length()) {
                    return 0;
                }
                if (stringIntegerEntry.getKey().length() < t1.getKey().length()) {
                    return -1;
                }
                return 1;
            }
        };
    }

    @Override
    public List<Map.Entry<String, Integer>> printUniqueWords() {
        List<Map.Entry<String, Integer>> listWord = new ArrayList<>(this.mapWord.entrySet());
        Collections.sort(listWord, keyComparator());
        return listWord;
    }

    @Override
    public List<Map.Entry<String, Integer>> printUniqueLines() {
        List<Map.Entry<String, Integer>> listLine = new ArrayList<>(this.mapLine.entrySet());
        Collections.sort(listLine, keyComparator());
        return listLine;
    }

    @Override
    public void frequencyOfWords() {
        System.out.println("Frequency of words in text");
        for(Map.Entry<String, Integer> entry : this.mapWord.entrySet()) {
            System.out.println("Word: " + entry.getKey() + " = " + entry.getValue());
        }
    }

    @Override
    public List<String> reverseLine() {
        List<String> list = new ArrayList<>();
        this.mapLine.forEach((k, v) -> {
            StringBuilder builder = new StringBuilder(k);
            builder.reverse();
            list.add(builder.toString());
        });
        return list;
    }

    @Override
    public void printByLine(String lines) {
        for (String num : lines.split(" ")) {
            System.out.println(this.listOfLines.get(Integer.valueOf(num)));
        }
    }


    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private Integer current = listOfLines.size() - 1;
            @Override
            public boolean hasNext() {
                if (this.current >= 0) {
                    return true;
                }
                return false;
            }

            @Override
            public String next() {
                return listOfLines.get(current--);
            }
        };
    }
}
