package org.hw1.cartype;

import org.hw1.Trade;

public class Pickup implements Trade {
    private Integer price;

    public Pickup(Integer price) {
        this.price = price;
    }

    public Pickup() {
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPrice() {
        return this.price;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getSimpleName()).append("\t").append(this.price);
        return builder.toString();
    }
}
