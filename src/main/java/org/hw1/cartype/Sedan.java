package org.hw1.cartype;

import org.hw1.Trade;

public class Sedan implements Trade {
    private Integer price;

    public Sedan(Integer price) {
        this.price = price;
    }

    public Sedan() {}

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPrice() {
        return this.price;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getSimpleName()).append("\t").append(this.price);
        return builder.toString();
    }
}
