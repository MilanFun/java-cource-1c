package org.hw1.enumcartype;

public enum ETrade implements ETradeInterface {
    HATCHBACK("HATCHBACK"),
    PICKUP("PICKUP"),
    SEDAN("SEDAN"),
    SUV("SUV");
    private final String carType;
    private ETrade(String carType) {
        this.carType = carType;
    }

    @Override
    public ETradeImpl createTrade(Integer price) {
        return new ETradeImpl(this, price);
    }
}
