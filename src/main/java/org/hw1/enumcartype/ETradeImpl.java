package org.hw1.enumcartype;

import org.hw1.Trade;

public class ETradeImpl implements Trade {
    private ETrade trade;
    private Integer price;

    public ETradeImpl(ETrade trade, Integer price) {
        this.trade = trade;
        this.price = price;
    }

    public ETradeImpl() {}

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPrice() {
        return this.price;
    }

    public void setTrade(ETrade trade) {
        this.trade = trade;
    }

    public ETrade getTrade() {
        return this.trade;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(trade).append("\t").append(this.price);
        return builder.toString();
    }
}
