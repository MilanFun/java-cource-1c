package org.hw1;

import org.hw1.cartype.*;
import org.hw1.enumcartype.ETrade;
import org.hw1.exception.NullElemOfTradeException;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void checkOnNull(String type, Integer price) {
        if (type == null) {
            throw new NullElemOfTradeException("Car is not recognised or not specified");
        }

        if (price == null) {
            throw new NullElemOfTradeException("Price is not recognised or not specified");
        }
    }

    public static Trade firstSolutionOnSwitch(String type, Integer price) {
        checkOnNull(type, price);
        switch (type) {
            case "HATCHBACK":
                return new Hatchback(price);
            case "SEDAN":
                return new Sedan(price);
            case "SUV":
                return new Suv(price);
            case "PICKUP":
                return new Pickup(price);
        }
        return null;
    }

    public static Trade secondSolutionOnEnum(String type, Integer price) {
        checkOnNull(type, price);
        return ETrade.valueOf(type).createTrade(price);
    }

    public static void main(String[] args) {
        String type = null;
        Integer price = null;
        try (Scanner scanner = new Scanner(System.in)) {
            File file = new File(scanner.nextLine());
            Scanner reader = new Scanner(file);
            while (reader.hasNextLine()) {
                String current = reader.nextLine();
                if (Constants.CarType.matcher(current).find()) {
                    type = current.trim().split("=")[1];
                }

                if (Constants.PRICE.matcher(current).find()) {
                    price = Integer.valueOf(current.trim().split("=")[1]);
                }
            }
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Switch solution:");
        Trade trade = firstSolutionOnSwitch(type, price);
        if (trade != null) {
            System.out.println(trade);
        } else {
            throw new NullElemOfTradeException("Trade of first solution on switch is null!");
        }

        Trade etrade = secondSolutionOnEnum(type, price);
        if (etrade != null) {
            System.out.println(etrade);
        } else {
            throw new NullElemOfTradeException("Trade of second solution on enum is null!");
        }
    }
}
