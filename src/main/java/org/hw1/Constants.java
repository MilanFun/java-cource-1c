package org.hw1;

import java.util.regex.Pattern;

public abstract class Constants {
    public static final String price = "\\d+";
    public static final String type = "HATCHBACK|SEDAN|SUV|PICKUP";
    public static final Pattern PRICE = Pattern.compile(Constants.price);
    public static final Pattern CarType = Pattern.compile(Constants.type);
}
