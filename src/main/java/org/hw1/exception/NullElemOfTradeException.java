package org.hw1.exception;

public class NullElemOfTradeException extends RuntimeException {
    public NullElemOfTradeException(String errorMessage) {
        super(errorMessage);
    }
}
